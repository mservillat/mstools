
from pylab import *
import numpy as np
import os,shutil
import smooth2D
import atpy

# ---------- XSPEC ----------

def xspec_cleandat(fname,path=''):
    if not os.path.isfile(path+fname+'.sav'):
        print 'Clean file from \"-\\n\"'
        shutil.copy(path+fname,path+fname+'.sav')
        fin =open(path+fname+'.sav')
        lines = fin.readlines()
        fin.close()
        fout=open(path+fname,'w')
        fout.write(''.join(lines).replace('-\n ',''))
        fout.close()

def xspec_plot_contours(fname,xrng,yrng,levels, levlab=['68%','90%','99%'], xlog=False,ylog=False, xlab='Parameter 1',ylab='Parameter 2', pltitle='', path='', overplot=False, color_contours=['k','r','g'], lw_contours=[3,3,3], ls_contours=['-','--',':'], legloc=3, savefig=False, smooth=False):
    # load file
    xspec_cleandat(fname,path=path)
    data = loadtxt(path+fname, skiprows=2)
    # smooth
    if smooth:
        import mstools
        data = mstools.smooth2D(data, boxsize=smooth)
    # prepare figure
    if overplot == False:
        fig = figure(figsize=(8,8))
        ax = fig.add_subplot(111)
    # plot
    sh = shape(data)
    xrngtmp = np.array(xrng)
    yrngtmp = np.array(yrng)
    if xlog: xrngtmp = np.log10(xrngtmp)
    if ylog: yrngtmp = np.log10(yrngtmp)
    dxy = [(xrngtmp[1]-xrngtmp[0])/(sh[0]-1), (yrngtmp[1]-yrngtmp[0])/(sh[1]-1)]
    X, Y = np.mgrid[xrngtmp[0]:xrngtmp[1]+dxy[0]/2.:dxy[0], yrngtmp[0]:yrngtmp[1]+dxy[1]/2.:dxy[1]]
    if xlog:
        X = 10**X
        ax.semilogx()
    if ylog: 
        Y = 10**Y
        ax.semilogy()
    ct = contour(X,Y,data,origin='lower', extent=(xrng[0],xrng[1],yrng[0],yrng[1]), aspect=(xrng[1]-xrng[0])/(yrng[1]-yrng[0]), levels=levels, linestyles=ls_contours, linewidths=lw_contours,  colors=color_contours, zorder=0)
    #clabel(ct, fontsize=14, inline=1, fmt='%1.2f', manual=True)
    ax.plot([0],[0], '-'+color_contours[0],lw=lw_contours[0],label=levlab[0])
    ax.plot([0],[0],'--'+color_contours[1],lw=lw_contours[1],label=levlab[1])
    ax.plot([0],[0], ':'+color_contours[2],lw=lw_contours[2],label=levlab[2])
    ax.minorticks_on()
    ax.set_xlim(xrng)
    ax.set_ylim(yrng)
    title(pltitle,fontsize='x-large')
    xlabel(xlab,size='x-large')
    ylabel(ylab,size='x-large')
    legend(title='Confidence Level',loc=legloc)
    if savefig:
        if savefig == True: savefig = fname.split('.')[0]+'.png'
        fig.savefig(savefig)
    return fig


#msplots.xspec_plot_contours('contours_age_fouthst.dat', [1e6,1e10], [1e-4,0.1], [47.17,49.48,54.08], xlog=True,ylog=True, legloc=4)

# ---------- DASCH ----------

# prepare DASCH file
def dasch_process_lc(fname,fsuffix='', keep_rng=False, aflags=False,aflags_reject=True, bflags=False,bflags_reject=True, qflags=False,qflags_reject=True, sigmaclip=False,medwin=31,detrend=False,lowessf=0.2):
    import mstools
    # load file
    lc = atpy.Table(fname)
    lc.sort('ExposureDate')
    lc.add_column('quality_int', array(lc.quality, dtype=int32))
    print 'Init:',len(lc)
    # reject flagged and high error points
    if aflags:
        for flg in aflags:
            if aflags_reject:
                lc = lc.where(lc.AFLAGS & 2**flg == 0)
            else:
                lc = lc.where(lc.AFLAGS & 2**flg > 0)
            print 'Aflag:',len(lc)
    if bflags:
        for flg in bflags:
            if bflags_reject:
                lc = lc.where(lc.BFLAGS & 2**flg == 0)
            else:
                lc = lc.where(lc.BFLAGS & 2**flg > 0)
            print 'Bflag:',len(lc)
    if qflags:
        for flg in qflags:
            if qflags_reject:
                lc = lc.where(lc.quality_int & 2**flg == 0)
            else:
                lc = lc.where(lc.quality_int & 2**flg > 0)
            print 'Qflag:',len(lc)
    lc = lc.where(lc.rejectFlag == 0)
    lc = lc.where(lc.magcal_local_error < 90.)
    # get important data
    lc.add_column('MJD', lc.ExposureDate - 2400000.5) # to MJD
    lc.add_column('mag', lc.magcal_magdep)
    lc.add_column('errmag', lc.magcal_local_error)
    flux = 10**(-0.4*lc.mag)
    lc.add_column('flux', flux/mean(flux))
    lc.add_column('errflux', lc.flux*lc.errmag)
    # cut parts
    if keep_rng:
        lc = lc.where((time > keep_rng[0])*(time < keep_rng[1]))
        print 'Keep interval '+str(keep_rng[0])+' to '+str(keep_rng[1])
    # sigma clip
    if sigmaclip:
        import scipy.signal
        lc.add_column('flux_med', scipy.signal.medfilt(lc.flux, medwin))
        lc.add_column('mag_med', scipy.signal.medfilt(lc.mag, medwin))
        sigma = median(lc.errflux)
        lc = lc.where( (abs(lc.flux - lc.flux_med) / sigma < sigmaclip) )
        print 'Clipping (sigma='+str(sigmaclip)+')'
    # detrend through R lowess fit
    if detrend:
        lc.add_column('flux_fit', mstools.lowess(xv,yv,f=lowessf))
        lc.add_column('flux_dtd', lc.flux / lc.flux_fit)
    # save result
    print 'End :',len(lc)
    fout = fname.split('.')[0]+'_processed'+fsuffix+'.fits'
    lc.write(fout,type='fits',overwrite=True)
    return fout

def aavso_process_lc(fname,fsuffix='',filt='B',offset=0,sigmaclip=False,medwin=51,abserr=0.02):
    lc2 = atpy.Table(fname,type='ascii',guess=False,delimiter=',')
    # filter
    selB = where(lc2.Band == filt)[0]
    lc2 = lc2.rows(selB)
    print 'Init:',len(lc2)
    noerr = where(lc2.Uncertainty == '')[0]
    #lc2 = lc2.rows(haveerr)
    lc2.Uncertainty[noerr] = array(['0']*len(noerr))
    # add columns
    lc2.add_column('MJD',lc2.JD-2400000.5)
    lc2.add_column('Unc', array(lc2.Uncertainty,dtype='float64'))
    # sigma clip
    lc2.add_column('mag', lc2.Magnitude)
    lc2.add_column('errmag', sqrt(lc2.Unc**2+abserr**2))
    flux = 10**(-0.4*lc2.mag)
    lc2.add_column('flux', flux/mean(flux))
    lc2.add_column('errflux', lc2.flux*lc2.errmag)
    if sigmaclip:
        import scipy.signal
        lc2.add_column('flux_med', scipy.signal.medfilt(lc2.flux, medwin))
        lc2.add_column('mag_med', scipy.signal.medfilt(lc2.mag, medwin))
        lc2_sigma = std(lc2.flux-lc2.flux_med)
        lc2 = lc2.where( (abs(lc2.flux - lc2.flux_med) / lc2_sigma < sigmaclip) )
    # save result
    print 'End :',len(lc2)
    fout = fname.split('.')[0]+'_processed'+fsuffix+'.fits'
    lc2.write(fout,type='fits',overwrite=True)
    return fout


def omc_process_lc(fname,fsuffix='',offset=0,sigmaclip=False,medwin=51,abserr=0.02):
    lc = atpy.Table(fname,type='fits')
    # filter
    print 'Init:',len(lc)
    # add columns
    lc.add_column('MJD',lc.BARYTIME+51544.)
    lc.add_column('Unc', array(lc.ERRMAG_V,dtype='float64'))
    # sigma clip
    lc.add_column('mag', lc.MAG_V)
    lc.add_column('errmag', sqrt(lc.Unc**2+abserr**2))
    flux = 10**(-0.4*lc.mag)
    lc.add_column('flux', flux/mean(flux))
    lc.add_column('errflux', lc.flux*lc.errmag)
    if sigmaclip:
        import scipy.signal
        lc.add_column('flux_med', scipy.signal.medfilt(lc.flux, medwin))
        lc.add_column('mag_med', scipy.signal.medfilt(lc.mag, medwin))
        lc_sigma = std(lc.flux-lc.flux_med)
        lc = lc.where( (abs(lc.flux - lc.flux_med) / lc_sigma < sigmaclip) )
    # save result
    print 'End :',len(lc)
    fout = fname.split('.')[0]+'_processed'+fsuffix+'.fits'
    lc.write(fout,type='fits',overwrite=True)
    return fout


# plot DASCH lc
def dasch_plot_lc(lc,overplot=False,color='b',marker='.',xlims=False,ylims=False, plotmean=True,ploterr=True, period=False, plotphase=False, plotbin=False,nbins=20, lc_aavso=False,aavso_box=False,aavso_xlims=False,aavso_color='g',aavso_marker='.', save=False):
    import sourcelists as sl
#    fname_pr = fname.split('.')[0]+'_processed.fits'
#    fname_aavso_pr = fname_aavso.split('.')[0]+'_processed.fits'
#    lc = atpy.Table(fname.split('.')[0]+'_processed.fits')
    if lc_aavso:
        lc2 = lc_aavso
    mjd = lc.MJD
    if plotphase:
        print 'Computing phase'
        mjd = (lc.MJD % period)
    # plot
    if not overplot:
        fig=figure(figsize=(10,6))
        subplots_adjust(left=0.08,right=0.92)
        ax = subplot(1,1,1)
    else:
        ax = overplot
    print 'Plotting',len(lc),'points'
    print ' rms =',std(lc.mag),'mean err =',mean(lc.errmag),'median err =',median(lc.errmag)
    if plotmean:
        ax.axhline(np.average(lc.mag,weights=lc.errmag),ls='--',color='k')
    if ploterr:
        ax.errorbar(mjd,lc.mag, yerr=lc.errmag, fmt=marker+color,capsize=0)
    else:
        ax.plot(mjd,lc.mag, marker+color)
    if lc_aavso:
        print 'Plotting',len(lc2),'points'
        print ' rms =',std(lc2.mag),'mean err =',mean(lc2.errmag),'median err =',median(lc2.errmag)
        ax.errorbar(lc2.MJD,lc2.mag, yerr=lc2.errmag, fmt=marker+aavso_color,capsize=0)
    if ylims:
        ax.set_ylim(ylims)
    else:
        ax.set_ylim(max(lc.mag+lc.errmag),min(lc.mag-lc.errmag))
    if xlims:
        ax.set_xlim(xlims)
    if not overplot:
        xlabel('Time (MJD)',size='x-large')
        ylabel('Magnitude',size='x-large')
        minorticks_on()
        # plot binned lightcurve
        if plotbin:
        #def foldlc(time,flux,errflux,pd=365.2422,bin=False,nbins=20,figsize=(10,6)):
            tfold = mod(lc.MJD,period)
            tdiv = divide(lc.MJD,period)
            bmin = arange(nbins)/float(nbins)*period
            bmax = (arange(nbins)+1)/float(nbins)*period
            for j in range(nbins):
                tbin = where((tfold >= bmin[j])*(tfold < bmax[j]))[0]
                errorbar(mean([bmin[j],bmax[j]]),mean(lc.mag[tbin]), xerr=period/nbins/2., yerr=median(lc.errmag[tbin]),color='k',lw=2)
            xlim(0,period)
            minorticks_on()
        # show year axis
        if not plotphase:
            mjdlim = xlim()
            yrmin = sl.mjd2years(mjdlim[0])
            yrmax = sl.mjd2years(mjdlim[1])
            ax_yr = twiny()
            ax_yr.ticklabel_format(useOffset=False)
            ax_yr.set_xlim([yrmin,yrmax])
            minorticks_on()
        else:
            #xlabel('Time (MJD)',size='x-large')
            ax_phase = twiny()
            ax_phase.ticklabel_format(useOffset=False)
            ax_phase.set_xlim([0,1])
            ax_phase.set_xlabel('Phase',size='x-large')
            minorticks_on()
    # show aavso lc zoom in a box
    if lc_aavso and aavso_box:
        ax2 = fig.add_axes(aavso_box) #[.52,.15,.36,.25]
        errorbar(lc2.MJD,lc2.mag,yerr=lc2.errmag,fmt=aavso_marker+aavso_color,capsize=0)
        minorticks_on()
        #xlim(min(lc2.MJD),max(lc2.MJD))
        ax2.set_ylim(max(lc2.mag+lc2.errmag),min(lc2.mag-lc2.errmag))
        if aavso_xlims:
            ax2.set_xlim(aavso_xlims)
        # show year axis
        mjdlim2 = ax2.get_xlim()
        yrmin2 = sl.mjd2years(mjdlim2[0])
        yrmax2 = sl.mjd2years(mjdlim2[1])
        ax_yr2 = ax2.twiny()
        ax_yr2.ticklabel_format(useOffset=False)
        ax_yr2.set_xlim([yrmin2,yrmax2])
        minorticks_on()
    if save:
        fout = save.split('.')[0]+'_lc'
        if plotphase:
            fout = fout+'_phase'
        print 'saving',fout
        savefig(fout+'.png')
        savefig(fout+'.eps')
        #savefig('hmxb_dasch_lc.eps')
    return ax


def pgram_plot(pinput,pgram,signi=False,overplot=False,plot_signi=True,figsize=(10,6),sbp_adjust=[0.8,0.6,0.8,.6],hspace=1.2, save=False, fsuffix='', xlab='Period (days)', ylab='Power', pllab=False, plines=False):
    if len(shape(pgram)) == 1:
        print 'convert inputs to list'
        pgram = [pgram]
        if signi: signi = [signi]
    npg = len(pgram)
    if not overplot:
        fig=figure(figsize=figsize)
        subplots_adjust(left=sbp_adjust[0]/figsize[0],bottom=sbp_adjust[1]/figsize[1],right=1.-sbp_adjust[2]/figsize[0],top=1.-sbp_adjust[3]/figsize[1],hspace=hspace/figsize[0])
    for i,pg in enumerate(pgram):
        ax=subplot(npg,1,i+1)
        semilogx(pinput,pg,'-',lw=3)
        if plines:
            for pl in plines:
                axvline(pl,ls=':',color='k')
        if signi and plot_signi:
            axhline(signi[i],ls='--',color='k')
        if pllab:
            text(0.05,0.75,pllab[i],transform=ax.transAxes)
#      ticklabel_format(style='plain')
        xticks([100,200,300,400,500,1000,2000,5000,10000,20000],[100,200,300,'',500,1000,2000,5000,10000,20000])
        xlim(min(pinput),max(pinput))
        ylabel(ylab,size='x-large')
        minorticks_on()
    xlabel(xlab,size='x-large')
    if save:
        fout = save.split('.')[0]+'_pgram'+fsuffix
        print 'saving',fout
        savefig(fout+'.png')
        savefig(fout+'.eps')





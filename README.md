# Python scripts for data analysis

I developped my own tools to handle sourcelists, convert coordinates and time, perform cross matching with KDTree and to create specific plots.

This kind of tool is now mutualized in the Astropy package, check out www.astropy.org!

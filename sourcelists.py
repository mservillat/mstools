#! /usr/bin/env python
#
# Functions to handle source lists and cross-matching of 2 or 3 source lists
#
# 10 Dec 2009: Mathieu Servillat
# 23 Fev 2011 v1

import os
import numpy as np
import atpy, asciitable
#from astLib import astCoords
#import astroTools

#---------- GENERAL ----------

def separation(ra1,dec1,ra2,dec2):
    """
    Give the separation in degrees between 2 points (ra1,dec1) or between a source list (ra1,dec1) and a reference point (ra2,dec2)

    Computation:
    given points: 1,2 and let point 3 be the north pole then the spherical astronomy cosine law is:
        cosc=cosa * cosb + sina * sinb * cosC (spherical astronomy, smart pg8)
    where:
        A (pnt 1) angle bewteen pnt 2,3
        B (pnt 2) angle between pnt 1,3
        C (pnt 3) angle between 1,2 just abs(ra1-ra2)
        a - distance  between b,Pole just (90- dec2)
        b - distance  between a,Pole just (90 -dec1)
        c - distance  between a,b  (what we want...)
    """
    #ra1=array([30.,31,32])
    #dec1=array([30.,31,32])
    #ra2=30.
    #dec2=30.
    ra1 = np.array(ra1)
    dec1 = np.array(dec1)
    anglC = np.deg2rad(ra1-ra2)
    dista = np.deg2rad(90-dec1)
    distb = np.deg2rad(90-dec2)
    cosc = np.cos(dista)*np.cos(distb)+np.sin(dista)*np.sin(distb)*np.cos(anglC)
    distc = np.rad2deg(np.arccos(cosc))
    return distc

def ra_shift(dec1,dist):
    """
    Give RA shift in degrees equivalent to distance dist in degrees for a given ra1 and dec1
    Remark: ra1 is not necessary for this

    Computation:
    given points: 1,2 and let point 3 be the north pole then the spherical astronomy cosine law is:
        cosc=cosa * cosb + sina * sinb * cosC (spherical astronomy, smart pg8)
    where:
        point 2 is at dec2 = dec1 and ra2 = ra1 + ra_shift where ra_shift corresponds to the given distance dist
        A (pnt 1) angle bewteen pnt 2,3
        B (pnt 2) angle between pnt 1,3
        C (pnt 3) angle between 1,2 (what we want...)
        a - distance  between b,Pole just (90- dec1)
        b - distance  between a,Pole just (90 -dec1)
        c - distance  between a,b given as dist
    """
    #dista = np.deg2rad(90-dec1)
    #distb = dista
    #distc = np.deg2rad(dist)
    #cosC = (np.cos(distc)-np.cos(dista)*np.cos(distb)) / (np.sin(dista)*np.sin(distb))
    #anglC = np.rad2deg(np.arccos(cosC))
    #return anglC
    return (dist)/np.cos(np.deg2rad(dec1))

#---------- READ/WRITE ----------

def save_ds9reg(filename,ra,dec,errors=.5,minerror=0.05, color='green',width=1,text='',tag='',header='fk5',other=''):
    """
    Save ra and dec as DS9 region file
    """
    n=len(ra)
    if type(errors) == type(1.): errors=np.array(n*[errors])
    sub = np.where(errors < minerror)
    if len(sub) > 0: errors[sub] = minerror
    if type(color) == type(''): color=n*[color]
    if type(width) == type(1): width=n*[width]
    if type(text) == type(''): text=n*[text]
    if type(tag) == type(''): tag=n*[tag]
    if type(other) == type(''): other=n*[other]
    fout=open(filename,'w')
    fout.write(header+'\n')
    for i in range(n):
        t=''
        if width[i] != 1: t+=' width='+str(width[i])+''
        if text[i] != '': t+=' text={'+str(text[i])+'}'
        if tag[i] != '': t+=' tag={'+str(tag[i])+'}'
        if other[i] != '': t+=' '+str(other[i])
        fout.write('circle('+str(ra[i])+','+str(dec[i])+','+str(errors[i])+'\") # color=\"'+color[i]+'\"'+t+'\n')
    fout.close()

def save_ds9reg_box(filename,ra,dec,h=1.,w=1., color='green',width=1,text='',tag='',header='fk5'):
    """
    Save ra and dec as DS9 region file with box(), h and w in arcmin
    text is added as a text() in the center of the box
    """
    n=len(ra)
    if type(h) == type(1.): h=np.array(n*[h])
    if type(w) == type(1.): w=np.array(n*[w])
    if type(color) == type(''): color=n*[color]
    if type(width) == type(1): width=n*[width]
    if type(text) == type(''): text=n*[text]
    if type(tag) == type(''): tag=n*[tag]
    fout=open(filename,'w')
    fout.write(header+'\n')
    for i in range(n):
        t=''
        if width[i] != 1: t+=' width='+str(width[i])+''
        if tag[i] != '': t+=' tag={'+str(tag[i])+'}'
        r = ra[i]
        if ':' in r: r = hms2deg(r)
        d = dec[i]
        if ':' in d: d = dms2deg(d)
        fout.write('box('+str(r)+','+str(d)+','+str(h[i])+'\','+str(w[i])+'\') # color=\"'+color[i]+'\"'+t+'\n')
        if text[i] != '':
            fout.write('# text('+str(r)+','+str(d+(h[i]/4.)/60.)+') text={'+str(text[i])+'}\n')
    fout.close()

def save_ds9reg_line(filename,ra1,dec1,ra2,dec2,color='green',text='',tag='',width=0):
    """
    Save ra and dec as DS9 region file with line()
    """
    n=len(ra1)
    if type(color) == type(''): color=n*[color]
    if type(text) == type(''): text=n*[text]
    if type(tag) == type(''): tag=n*[tag]
    if type(width) == type(0): width=n*[width]
    fout=open(filename,'w')
    fout.write('fk5\n')
    for i in range(n):
        t=''
        if text[i] != '': t+=' text={'+str(text[i])+'}'
        if tag[i] != '': t+=' tag={'+str(tag[i])+'}'
        if width[i] != 0: t+=' width='+str(width[i])
        fout.write('line('+str(ra1[i])+','+str(dec1[i])+','+str(ra2[i])+','+str(dec2[i])+') # color=\"'+color[i]+'\"'+t+'\n')
    fout.close()

def read_ds9reg(filename,shape='circle',radius=True,type='atpy'):
    """
    Save ra and dec as DS9 region file
    If coordinates are in h:m:s d:m:s format it is converted to degrees
    type='atpy' gives an atpy table
    return ra,dec,r,text
    """
    fin=open(filename)
    ra=[]
    dec=[]
    r=[]
    r2=[]
    text=[]
    for line in fin.readlines():
        if shape in line:
            coords=line.split(')')[0].split('(')[-1].split(',')
            #print coords
            if ':' in coords[0]:
                rai = hms2deg(coords[0].strip())
            else:
                rai = float(coords[0])
            if ':' in coords[1]:
                deci = dms2deg(coords[1].strip())
            else:
                deci = float(coords[1])
            ra.append(rai)
            dec.append(deci)
            if '"' in coords[2]:
                ri = float(coords[2][0:-1])/3600.
            else:
                if '\'' in coords[2]:
                    ri = float(coords[2][0:-1])/60.
                else:
                    ri = float(coords[2])
            r.append(ri)
            if shape == 'ellipse' or shape == 'box':
                if '"' in coords[3]:
                    r2i = float(coords[3][0:-1])/3600.
                else:
                    if '\'' in coords[3]:
                        r2i = float(coords[2][0:-1])/60.
                    else:
                        r2i = float(coords[3])
                r2.append(r2i)
            if 'text={' in line:
                text.append(line.split('text={')[-1].split('}')[0])
            else:
                text.append('')
    if shape == 'ellipse':
        r=list(np.mean(zip(np.array(r),np.array(r2)),axis=1))
    if type == 'atpy':
        t = atpy.Table()
        t.add_column('RA',ra)
        t.add_column('Dec',dec)
        t.add_column('err',r)
        t.add_column('text',text)
        return t
    else:
        return ra,dec,r,text

def rdb_read(filename,delimiter='\t',comments='#',data_start=2,verbose=False):
    read_rdb(*args, **kwargs)

def read_rdb(filename,delimiter='\t',comments='#',data_start=2,verbose=False):
    """
    Read a RDB acsii file (see starbase project)
    """
    tab=atpy.Table()
    tab.table_name=filename.split('/')[-1]
    data = asciitable.read(filename, Reader=asciitable.RdbReader, data_start=data_start, delimiter=delimiter)
    for c in data.dtype.names:
        tab.add_column(c,data[c])
    if verbose: tab.describe()
    return tab


def write_refcat_scamp(filename,ra,dec,erra,errb,mag,magerr):
    out  = atpy.TableSet()
    # create imhead table
    out1 = atpy.Table()
    out1.table_name = 'LDAC_IMHEAD'
    out1.add_column('Field Header Card',['SIMPLE  =                    T / This is a FITS file'],dtype='|S1680')
    out.append(out1)
    # create objects table
    out2 = atpy.Table()
    out2.table_name = 'LDAC_OBJECTS'
    out2.add_column('X_WORLD',ra,unit='deg')
    out2.add_column('Y_WORLD',dec,unit='deg')
    out2.add_column('ERRA_WORLD',erra,dtype='float32',unit='deg')
    out2.add_column('ERRB_WORLD',errb,dtype='float32',unit='deg')
    out2.add_column('MAG',mag,dtype='float32',unit='mag')
    out2.add_column('MAGERR',magerr,dtype='float32',unit='mag')
    out.append(out2)
    # save new file, ready for scamp
    out.write(filename,overwrite=True)


def read_obscat_magellan(filename,verbose=False):
    """
    Read a Magellan obs catalog from acsii file into atpy table
    """
    tab=atpy.Table()
    tab.table_name=filename.split('/')[-1]
    data = asciitable.read(filename, names=['num','name','RA','Dec','epoch', 'RApm','Decpm','offset','rot','RA_probe1','Dec_probe1','epoch_probe1','RA_probe2','Dec_probe2','epoch_probe2'],data_start=0,comment='#',delimiter=' ')
    for c in data.dtype.names:
        tab.add_column(c,data[c])
    if verbose: tab.describe()
    return tab


#----- MERGE -----

def add_colaffix(table, prefix='', suffix=''):
    for c in table.columns.keys:
        newc = prefix+c+suffix
        if newc != c:
            table.rename_column(c,newc)


def add_columns(colnames, table1, table2, prefix='', suffix=''):
    """
    Add columns from table2 to table1. If the column exists, it is skipped. Columns must have same length
    - colnames: list of column names
    - table1: atpy table
    - table2: atpy table with same number and order of rows
    - prefix='': add a prefix to the column names (useful when column name already exist in a different context)
    """
    for c in colnames:
        newc = prefix+c+suffix
        cols = table1.columns.keys
        if newc not in cols:
            table1.add_column(newc,table2[c])
            print 'Column '+newc+' added'
        else:
            print '! Column '+c+' already exists'


def add_columns_all(table1, table2, prefix='', suffix=''):
    add_columns(table2.columns.keys, table1, table2, prefix=prefix, suffix=suffix)


def match_columns(table1, table2, col1, col2=None):
    """
    Return indices i1 in table1 corresponding to indices i2 in table2 using a column "col1" as a unique index.
    """
    if col2 == None: col2=col1
    i1=[]
    i2=[]
    for i,v in enumerate(table1[col1]):
        if v in table2[col2]:
            i1.append(i)
            ii = np.where(table2[col2] == v)[0]
            if len(ii) > 1:
                print 'Warning: index is not unique for '+col1+'='+str(v)+' (row='+str(i)+')'
            i2.append(ii[0])
    return i1, i2


def add_subcolumn(colname, table1, i1, colvalues, init=None):
    """
    Add given column to table1 at the given i1 indices. If the column exists, it is skipped.
    - colname: column name
    - table1: atpy table
    - i1: list of indices
    - colvalues: array of values with same length as i1
    - init=None: default value for missing values
    """
    if colname in table1.columns.keys:
        print 'Column '+colname+' already exists'
    else:
        dtype = colvalues.dtype
        a = np.zeros(len(table1), dtype)
        if init != None: a.fill(init)
        table1.add_column(colname, a, dtype=dtype)
        table1[colname][i1] = colvalues


def add_subcolumns(colnames, table1, i1, table2, i2, prefix='', newnames=None):
    '''
    Add colnames to table1 at indices i1, from table2 at indices i2. Generally i1 and i2 come from match_columns().
    - colnames: list of column names
    - table1: atpy table
    - i1: list of indices for table1
    - table2: atpy table
    - i2: list of indices for table2, same length as i1
    - prefix='': add a prefix to the column names (useful when column name already exist in a different context)
    - newnames=None: list of new column names, same length as colnames
    '''
    if len(i1) == len(i2):
        for i,colname in enumerate(colnames):
            newname = prefix+colname
            if newnames: newname=newnames[i]
            add_subcolumn(newname, table1, i1, table2[colname][i2])
    else:
        print 'Error: i1 and i2 must have same length'


def add_subcolumns_all(table1, i1, table2, i2, prefix='', newnames=None):
    add_subcolumns(table2.columns.keys, table1, i1, table2, i2, prefix=prefix, newnames=newnames)


def show(table,rows='all',cols='all',horiz=True,tofile=None,fmt='stdout',errorcol=[],roundcol={},scale={},nodata={}):
    '''
    Print a row or several rows, with all or specified columns in an easy to read format.
    Print in a vertical way or in an horizontal way (horiz=True).
    '''
    print ''
    if rows == 'all':
        rows = range(len(table))
    if cols == 'all':
        cols = table.columns.keys
    if type(rows)==type(1):
        rows=[rows]
    if type(cols)==type(' '):
        cols=[cols]
    if tofile:
        f = open(tofile,'w')
    if horiz:
        #find length
        len_max = [4]*len(cols)
        for row in table.rows(rows):
            for i,c in enumerate(cols):
                val = row[c]
                if c in scale.keys(): val = float(val)*scale[c]
                if c in roundcol.keys(): val = round(float(val),roundcol[c])
                if c in nodata.keys() and val==nodata[c]: val='\\nodata'
                len_max[i] = max([len(str(val)), len_max[i], len(c)])
        format1 = '%-'+'s  %-'.join(list(np.array(len_max,dtype='S5')))+'s'
        format2 = '%' +'s  %'.join(list(np.array(len_max,dtype='S5')))+'s'
        # change format for latex tables
        if fmt == 'latex':
            format1 = '' #'%-'+'s & %-'.join(list(np.array(len_max,dtype='S5')))+'s \\\\'
            format2 = '' #'%' +'s & %'.join(list(np.array(len_max,dtype='S5')))+'s \\\\'
            for i in range(len(cols)):
                if i > 0 and i not in errorcol :
                    format1 += '& '
                    format2 += '& '
                fmtelt1 = '%-'+str(len_max[i])+'s '
                fmtelt2 = '%-'+str(len_max[i])+'s '
                if i+1 in errorcol:
                    fmtelt1 = ' '+fmtelt1+'      '
                    fmtelt2 = ' '+fmtelt2+'$\pm$ '
                format1 += fmtelt1
                format2 += fmtelt2
            format1 += '\\\\'
            format2 += '\\\\'
            # latex header format for deluxe tables
            format1b = '\\tablehead{\\colhead{%'+'s} & \\colhead{%-'*(len(cols)-1)+'s}}'
            print format1b % tuple(cols)
            print ''
        # print table
        print format1 % tuple(cols)
        print format2 % tuple(['-'*l for l in len_max])
        #print '-'*(np.sum(len_max)+(len(len_max))-1)
        if tofile:
            f.write(format1 % tuple(cols))
            f.write('\n')
            f.write(format2 % tuple(['-'*l for l in len_max]))
            f.write('\n')
        vnext=1
        for i in rows:
            line=[]
            for j,c in enumerate(cols):
                val=table[c][i]
                if c in scale.keys(): val = float(val)*scale[c]
                if c in roundcol.keys(): 
                    val = round(float(val),roundcol[c])
                    if roundcol[c] == 0: val=int(val)
                if vnext == 0: val = ' '*len_max[j]
                vnext = 1
                if c in nodata.keys() and val==nodata[c]: 
                    val='\\nodata'
                    vnext = 0
                line.append(val)
            print format2 % tuple(line)
            if tofile:
                f.write(format2 % tuple(line))
                f.write('\n')
        #print '-'*(np.sum(len_max)+(len(len_max)*3)-1)
    else:
        #find length
        len_max = 4
        for col in cols:
            len_max = max(len(col), len_max)
        format = '%'+str(len_max)+'s = %s'
        for c in cols:
            print format % (c, str(table[c][rows]))
            if tofile:
                f.write(format % (c, str(table[c][rows])))
                f.write('\n')
    if tofile:
        f.close()


#---------- CONVERT COORD ----------
# from astroTools, or use atropysics

import time,calendar
import string

def isarray(obj):
    if hasattr(obj,'shape'):
        s=obj.shape
        if len(s) > 0: return True
        else: return False
    elif hasattr(obj,'pop'):
        return True
    return False

# Convert HH:MM:SS.SSS into Degrees :
def hms2deg(ra,separator=':'):
    if isarray(ra):
        result=[]
        for rai in ra:
            result.append(_hms2deg(rai,separator=separator))
        return np.array(result,dtype='float64')
    else:
        return _hms2deg(ra,separator=separator)

def _hms2deg(ra,separator=':'):
   try :
      sep1 = ra.find(separator)
      hh=int(ra[0:sep1])
      sep2 = ra[sep1+1:].find(separator)
      mm = int(ra[sep1+1:sep1+sep2+1])
      ss = float(ra[sep1+sep2+2:])
   except:
      raise
   else:
      pass
   return(hh*15.+mm/4.+ss/240.)

# Convert +DD:MM:SS.SSS into Degrees :
def dms2deg(dec,separator=':'):
    if isarray(dec):
        result=[]
        for deci in dec:
            result.append(_dms2deg(deci,separator=separator))
        return np.array(result,dtype='float64')
    else:
        return _dms2deg(dec,separator=separator)

def _dms2deg(dec,separator=':'):
   Csign=dec[0]
   if Csign == '-':
      sign=-1.
      off = 1
   elif Csign == '+':
      sign= 1.
      off = 1
   else:
      sign= 1.
      off = 0
   try :
      sep1 = dec.find(separator)
      deg = int(dec[off:sep1])
      sep2 = dec[sep1+1:].find(separator)
      arcmin = int(dec[sep1+1:sep1+sep2+1])
      arcsec = float(dec[sep1+sep2+2:])
   except:
      raise
   else:
      pass
   return(sign*(deg+(arcmin*5./3.+arcsec*5./180.)/100.))

# Convert RA (deg) to H.M.S:
def deg2hms(RAin,separator=':',plus='',digits=3):
    if isarray(RAin):
        result=[]
        for RAini in RAin:
            result.append(_deg2hms(RAini,separator=separator,plus=plus))
        return np.array(result)
    else:
        return _deg2hms(RAin,separator=separator,plus=plus)

def _deg2hms(RAin,separator=':',plus='',digits=3):

   if(RAin<0):
      sign = -1
      ra   = -RAin
   else:
      sign = 1
      ra   = RAin

   h = int( ra/15. )
   ra -= h*15.
   m = int( ra*4.)
   ra -= m/4.
   s = ra*240.

   if(sign == -1):
      out = ('-%02d'%h)+separator+('%02d'%m)+separator+(('%0'+str(digits+3)+'.'+str(digits)+'f')%s)
      #+str(round(s,digits)).zfill(3+digits)
   else: out = plus+('%02d'%h)+separator+('%02d'%m)+separator+(('%0'+str(digits+3)+'.'+str(digits)+'f')%s)
   #+str(round(s,digits)).zfill(3+digits)

   return out

# Convert Decl. (deg) to D.M.S:
def deg2dms(Decin,separator=':',plus='',digits=2):
    if isarray(Decin):
        result=[]
        for Decini in Decin:
            result.append(_deg2dms(Decini,separator=separator,plus=plus))
        return np.array(result)
    else:
        return _deg2dms(Decin,separator=separator,plus=plus)

def _deg2dms(Decin,separator=':',plus='',digits=2):

   if(Decin<0):
      sign = -1
      dec  = -Decin
   else:
      sign = 1
      dec  = Decin

   d = int( dec )
   dec -= d
   dec *= 100.
   m = int( dec*3./5. )
   dec -= m*5./3.
   s = dec*180./5.

   if(sign == -1):
      out = ('-%02d'%d)+separator+('%02d'%m)+separator+(('%0'+str(digits+3)+'.'+str(digits)+'f')%s)
      #+str(round(s,digits)).zfill(3+digits)
   else: out = plus+('%02d'%d)+separator+('%02d'%m)+separator+(('%0'+str(digits+3)+'.'+str(digits)+'f')%s)
   #+str(round(s,digits)).zfill(3+digits)

   return out

#---------- TIME CONVERSION

import astropysics.obstools
import datetime

# MIDAS: MJD @ MMJD=0 :
# MMJD = MJD - 54640 ; soit MMJD=1 a 00:00 01/01/2003
# MJD=0 a 00:00 17/11/1858
mjd2mmjd = -52640.0
mmjd2gmt = 12052.0
# MJD = JD - 2400000.5 ; soit JD=0 a 12:00 January 1, 4713 BC, Monday
mjd2jd = 2400000.5
jd2mjd = -2400000.5
# MJD = RJD - 0.5 ; soit RJD=0 a 12:00 16/11/1858
mjd2rjd = 0.5
rjd2mjd = -0.5
jd2rjd = 2400000.
rjd2jd = -2400000.

# from astrotools
def _dateobs2mmjd(dateobs,separator='-'):
    tuple_time = time.strptime(dateobs, "%Y"+separator+"%m"+separator+"%d" )
    dateobs_as_seconds = time.mktime(tuple_time)
    #refepoch = time.strptime("2003-01-01", "%Y-%m-%d" )
    refepoch = time.strptime("2003-01-01", "%Y-%m-%d" )
    refepoch_as_seconds = time.mktime(refepoch)

    mjd = (dateobs_as_seconds - refepoch_as_seconds) /3600. /24.
    #print mmjd,round(mmjd)
    return round(mjd)

def dateobs2mmjd(dateobs,separator='-'):
    if isarray(dateobs):
        result=[]
        for dateobsi in dateobs:
            result.append(_dateobs2mmjd(dateobsi,separator=separator))
        return np.array(result,dtype='int64')
    else:
        return _dateobs2mmjd(dateobs,separator=separator)
def _timeobs2mjd(timeobs,separator='T'):
   day = timeobs[0:timeobs.find(separator)]
   time= timeobs[timeobs.find(separator)+1:]
   try :
      hour= int(time[0:2])
      min = int(time[3:5])
      sec = float(time[6:])
   except :
      raise 'Format', timeobs
   else :
      mjd = dateobs2mjd(day)
      mjd += (((sec/60.)+min)/60.+hour)/24.

   #print hour,min,sec,(((sec/60.)+min)/60.+hour)/24.
   return mjd

def timeobs2mmjd(timeobs,separator='T'):
    return timeobs2mjd(timeobs,separator=separator)+mjd2mmjd

def timeobs2secUT(timeobs):

   try :
      tuple = time.strptime(timeobs[0:19], '%Y-%m-%dT%H:%M:%S')
   except :
      raise 'Format', timeobs
   else :
      secs = time.mktime(tuple)

   return secs

# using astropysics with strings as inputs
def _dateobs2jd(dateobs,separator='-'):
    d1 = dateobs.split(separator)
    d2 = (int(d1[0]),int(d1[1]),int(d1[2]))
    jd = astropysics.obstools.calendar_to_jd(d2)
    return jd

def dateobs2jd(dateobs,separator='-'):
    if isarray(dateobs):
        result=[]
        for dateobsi in dateobs:
            result.append(_dateobs2jd(dateobsi,separator=separator))
        return np.array(result,dtype='int64')
    else:
        return _dateobs2jd(dateobs,separator=separator)

def dateobs2mjd(dateobs,separator='-'):
    jd = dateobs2jd(dateobs,separator=separator)
    mjd = jd + jd2rjd
    return mjd

def _timeobs2jd(timeobs,dtsep='T',dsep='-',tsep=':'):
    dt1 = timeobs.split(dtsep)
    d1 = dt1[0].split(dsep)
    t1 = dt1[1].split(tsep)
    dt2 = (int(d1[0]),int(d1[1]),int(d1[2]),int(t1[0]),int(t1[1]),float(t1[2]))
    jd = astropysics.obstools.calendar_to_jd(dt2)
    return jd

def timeobs2jd(timeobs,dtsep='T',dsep='-',tsep=':'):
    if isarray(timeobs):
        result=[]
        for timeobsi in timeobs:
            result.append(_timeobs2jd(timeobsi,dtsep=dtsep,dsep=dsep,tsep=tsep))
        return np.array(result,dtype='float64')
    else:
        return _timeobs2jd(timeobs,dtsep=dtsep,dsep=dsep,tsep=tsep)

def timeobs2mjd(timeobs,dtsep='T',dsep='-',tsep=':'):
    jd = timeobs2jd(timeobs,dtsep=dtsep,dsep=dsep,tsep=tsep)
    mjd = jd + jd2mjd
    return mjd

def _jd2time(jd,dtsep='T',dsep='-',tsep=':',secdec=False):
    d1 = astropysics.obstools.jd_to_calendar(jd)
    #d2 = str(d1[0])+dsep+str(d1[1]).zfill(2)+dsep+str(d1[2]).zfill(2)+dtsep++str(d1[3]).zfill(2)+tsep+str(d1[4]).zfill(2)+tsep+str(d1[5]).zfill(2)
    d2 = d1.__str__().split('+')[0]
    d3 = d2.replace(' ',dtsep)
    if secdec:
        secfrac = modf(jd*86400)[0]
        d3+='.'+str(round(secfrac,3)).split('.')[1]
    return d3

def jd2time(jd,dtsep='T',dsep='-',tsep=':'):
    if isarray(jd):
        result=[]
        for jdi in jd:
            result.append(_jd2time(jdi,dtsep=dtsep,dsep=dsep,tsep=tsep))
        return np.array(result,dtype='string')
    else:
        return _jd2time(jd,dtsep=dtsep,dsep=dsep,tsep=tsep)

def mjd2time(mjd,dtsep='T',dsep='-',tsep=':'):
    return jd2time(np.array(mjd)+mjd2jd,dtsep=dtsep,dsep=dsep,tsep=tsep)

def mjd2years(mjd):
    time = mjd2time(mjd)
    yyyy,mm,dd = time.split('T')[0].split('-')
    #diff = timeobs2mjd(yyyy+'')
    return ((float(dd)-1)/31.+float(mm)-1)/12.+float(yyyy)

# MMJD -> Date YYYY-MM-DD & HH:MM
def mmjd2date( mmjd ):
    refepoch = time.strptime("2003-01-01", "%Y-%m-%d" )
    refepoch_as_seconds = time.mktime(refepoch)
    epoch = refepoch_as_seconds + mmjd*24.*60.*60.
    tuple_date = time.gmtime( epoch )
    date = time.strftime('%Y-%m-%dT%H:%M', tuple_date)
    day = date[:date.find('T')]
    hour= date[date.find('T')+1:]
    return [day, hour]


# Cut YYYY-MM-DDTHH:MM:SS.SSS into [Y,M,D,H,M,S]:
def splitESOdate( timeobs ):

   date = timeobs[0:timeobs.find('T')]
   time= timeobs[timeobs.find('T')+1:]
   try :
      year   = int( date[:4]  )
      mounth = int( date[5:7] )
      day    = int( date[8:10] )
      hour= int(time[0:2])
      min = int(time[3:5])
      sec = float(time[6:])
   except :
      print '! Bad Obs Date Format : %s .'%timeobs
      raise
   else :
      pass

   return [year, mounth, day, hour, min, sec ]

# Return the ObsDate at ESO convention :  date of beginning of night.
# Use GMT to handle change in month :
def getESOnight( timeobs ):

   try :
      [y,m,d,h,min,sec] = splitESOdate( timeobs)
   except:
      print '! Not ESO date format : %s '%timeobs
      raise
   else :
      if h < 12 : corr = -1.
      else : corr = 0.

   day = '%s-%s-%s'%(y,m,d)
   tuple   = time.strptime(day,'%Y-%m-%d')
   sec     = calendar.timegm(tuple)+corr*24.*60.*60.
   ESOtime = time.gmtime(sec)
   ESOday  = time.strftime('%Y-%m-%d', ESOtime)

   return ESOday


# Return the hour modulus 12. from HH:MM:SS :
def ESOhour ( hour ):
   try :
      h = int(hour[:2])
      m = int(hour[3:5])
      s = int(hour[6:8])
   except:
      print '! Format error : %s .'%hour
      raise

   ESOhour = h+m/60.+s/3600.

   if ESOhour > 12. : ESOhour -= 24.

   return ESOhour





#---------- MATCHING ----------

def match2(ra1,dec1,ra2,dec2,errors=.2,nsigma=1.,radius=.2,onlybest=False,bsra=0,bsdec=0):
    """
    Match 2 (ra,dec) source lists and return the indices and separations in arcsec
    Give radius or individual errors for the second list
    Should be faster if the second sourcelist is shorter than the first

    Input:
    - ra1, dec1: first source list with RA and Dec in degrees
    - ra2, dec2: second source list with RA and Dec in degrees
    - radius: distance in arcsec for the most distant match
    - errors: distance in arcsec for the most distant match for each source in the second list, if given, radius won't be used
    - onlybest: if True, will keep only the best match, if False will list all the matches within the given error or radius
    - bsra, bsdec: boresight correction in arcsec

    Output:
    - ind1, ind2: indices for each source list for successfull matches between the 2 source lists
    - dist: array with same number of elements with the separations in arcsec
    """
    from scipy.spatial import KDTree
    ra1=np.array(ra1)
    dec1=np.array(dec1)
    ra2=np.array(ra2)
    dec2=np.array(dec2)
    n1=len(ra1)
    n2=len(ra2)
    decmd = np.median(np.append(dec1,dec2))
    decmd_rad = np.deg2rad(decmd)
    #ra_factor =  (1/60) / ra_shift(decmd,1/60)
    # apply boresight correction
    bsra_shift = 0.
    bsdec_shift = 0.
    if bsra != 0:
        bsra_shift = (bsra/3600)/np.cos(decmd_rad)
    if bsdec != 0:
        bsdec_shift += bsdec/3600
    print '\nMatching 2 lists of',n1,'and',n2,'elements'
    tree1=KDTree(zip(ra1*np.cos(decmd_rad),dec1))
    pts=np.array(zip((ra2+bsra_shift)*np.cos(decmd_rad), dec2+bsdec_shift))
    #tree2=KDTree(zip(ra2*ra_factor,dec2))
    if errors == 'none': errors = radius
    if 'float' in str(type(errors)):
        errors=n2*[float(errors)]
    errors=np.array(errors)
    if onlybest:
        print 'Keep only best matches'
        ind1=[]
        ind2=[]
        dist12=[]
        for i in range(len(pts)):
            md,mi=tree1.query(pts[i], distance_upper_bound=errors[i]/3600)
            if mi < n1:
                ind1.append(mi)
                ind2.append(i)
                dist12.append(separation(ra1[mi],dec1[mi],ra2[i],dec2[i])*3600)
    else:
        ind1=[]
        ind2=[]
        dist12=[]
        for i in range(len(pts)):
            mi=[0]
            k=2
            while mi[-1] != n1:
                md,mi=tree1.query(pts[i], k=k, distance_upper_bound=errors[i]/3600)
                k+=2
            for j in range(len(mi)):
                if mi[j] < n1:
                    ind1.append(mi[j])
                    ind2.append(i)
                    dist12.append( separation(ra1[mi[j]],dec1[mi[j]],ra2[i],dec2[i])*3600 )

    print 'Found',len(ind1),'matches'
    return np.array(ind1),np.array(ind2),np.array(dist12)


#----------
def match2_boresight(ra1,dec1,ra2,dec2,errors=.2,nsigma=1., bsra_init=0.,bsdec_init=0.,outfile=False,nsigmasub=None,labels=None):
    """
    Call match2 and analyse the results to estimate the boresight shift and error from the weighted mean of the difference ra1-ra2 and dec1-dec2

    Input:
    - ra1, dec1: first source list with RA and Dec in degrees
    - ra2, dec2: second source list with RA and Dec in degrees
    - errors: 1 sigma error in arcsec for each source in the second list
    - nsigma: multiply errors by nsigma for matching
    - bsra_init: initial RA correction in arcsec for the second source list (ra2+bsra_init_shift)
    - bsdec_init: initial Dec correction in arcsec for the second source list (dec2+bsdec_init)

    Output:
    - ind1, ind2: indices for each source list for successfull matches between the 2 source lists
    - dist: array with same number of elements with the separations in arcsec
    - bsra, bsdec: boresight corection in arcsec
    - bsrasig, bsdecsig: 1 sigma error on boresight correction
    """
    # correct rashift distance to rashiftd angle !!! Necessary? give angle as input for now
    ra1=np.array(ra1)
    dec1=np.array(dec1)
    ra2=np.array(ra2)
    dec2=np.array(dec2)
    n1=len(ra1)
    n2=len(ra2)
    bsra_init_shift = 0.
    decmd = np.median(np.append(dec1,dec2))
    decmd_rad = np.deg2rad(decmd)
    if bsra_init != 0:
        bsra_init_shift = (bsra_init/3600.)/np.cos(decmd_rad)
    if 'float' in str(type(errors)):
        errors=n2*[float(errors)]
    errors=np.array(errors)
    if nsigmasub == None: nsigmasub = nsigma
    # match source lists
    ind1,ind2,dist12 = match2(ra1, dec1, ra2, dec2, errors=errors*nsigma, onlybest=True,bsra=bsra_init,bsdec=bsdec_init)
    # weighted mean for RA and Dec
    nxi = len(ind2)
    bsra = 0
    bsdec = 0
    bsrasig = 0
    bsdecsig = 0
    if nxi > 0:
        wi = (1./errors[ind2]**2)
        sumwi = np.sum(wi)
        radist_shift = (ra1[ind1] - (ra2[ind2]+bsra_init_shift))
        radist = radist_shift*np.cos(decmd_rad)
        bsra_shift = np.sum(wi*radist_shift) / sumwi
        bsra = bsra_shift*np.cos(decmd_rad)
        decdist = (dec1[ind1] - (dec2[ind2]+bsdec_init/3600.))
        bsdec = (np.sum(wi*decdist) / sumwi)
        # sigma for weighted mean
        bsrasig_shift = np.sqrt(np.sum(wi*(radist_shift-bsra_shift)**2) / ((nxi-1)*sumwi))
        bsrasig = bsrasig_shift*np.cos(decmd_rad) * 3600.
        bsdecsig = (np.sqrt(np.sum(wi*(decdist-bsdec)**2) / ((nxi-1)*sumwi))) * 3600.
        #bserr = np.sqrt(bsrasig**2+bsdecsig**2)
        bsra *= 3600.
        bsdec *= 3600.
        print 'Found boresight correction: '+str(round(bsra,5))+' +/- '+str(round(bsrasig,5))+', '+str(round(bsdec,5))+' +/- '+str(round(bsdecsig,5))+' arcsec'

        if outfile:
            from matplotlib import cm
            import matplotlib.pyplot as plt
            fig = plt.figure(figsize=(8,8))
            ax = plt.subplot(111)
            titadd = ''
            if (bsra_init != 0.) or (bsdec_init != 0):
                titadd = '\n(bsra='+str(bsra_init)+',bsdec='+str(bsdec_init)+')'
            ax.set_title(os.path.basename(outfile)+titadd)
            p1 = ax.plot(radist*3600.,decdist*3600.,'.b',ms=10)
            if nsigmasub != nsigma:
                ind1s,ind2s,dist12s = match2(ra1[ind1], dec1[ind1], ra2[ind2], dec2[ind2], errors=errors[ind2]*nsigmasub, onlybest=True,bsra=bsra_init,bsdec=bsdec_init)
                p2 = ax.plot(radist[ind1s]*3600.,decdist[ind1s]*3600.,'.r',ms=10)
                ax.legend( (p1,p2), ('< '+str(nsigma)+' sigma matches','< '+str(nsigmasub)+' sigma matches'), 'upper right', numpoints=1, prop={'size':11})
            else:
                ax.legend( (p1), ('< '+str(nsigma)+' sigma matches'), 'upper right', numpoints=1, prop={'size':11})
            xymax = round(max(np.append(np.abs(radist),np.abs(decdist)))*36000.)/10.+0.1
            lim = [-xymax,xymax]
            xl = ax.set_xlabel('RA shift (arcsec)')
            yl = ax.set_ylabel('Dec shift (arcsec)')
            plx = ax.plot([0,0],lim,'--k')
            ply = ax.plot(lim,[0,0],'--k')
            xlim = ax.set_xlim(lim)
            ylim = ax.set_ylim(lim)
            g = ax.grid()
            plt.savefig(outfile)

    return ind1,ind2,dist12, bsra, bsdec, bsrasig, bsdecsig, nxi


#----------
def match2_boresight_loop(ra1,dec1,ra2,dec2,errors=.2,nsigma=1., bsra=0.,bsdec=0.,bserr=.4,precision=0.001):
    """
    Iterate to get boresight correction with given precision

    Input:
    - ra1, dec1: first source list with RA and Dec in degrees
    - ra2, dec2: second source list with RA and Dec in degrees
    - errors: 1 sigma error in arcsec for each source in the second list
    - nsigma: multiply errors by nsigma for matching
    - bsra: initial RA correction in arcsec for the second source list
    - bsdec: initial Dec correction in arcsec for the second source list
    - bserr: initial boresight 1 signa error in arcsec for the second source list (i.e. the absolute error). To be added to error (which should contain all the other relative errors)
    - precision: refine boresight until the correction is lower than precision

    Output:
    - ind1, ind2: indices for each source list for successfull matches between the 2 source lists
    - dist: array with same number of elements with the separations in arcsec
    - bsra_tot, bsdec_tot: total boresight corection in arcsec
    - bsrasig, bsdecsig: 1 sigma error on boresight correction
    """
    ra1=np.array(ra1)
    dec1=np.array(dec1)
    ra2=np.array(ra2)
    dec2=np.array(dec2)
    n1=len(ra1)
    n2=len(ra2)
    if 'float' in str(type(errors)):
        errors=n2*[float(errors)]
    errors=np.array(errors)
    bsra_tot=bsra
    bsdec_tot=bsdec
    bsra=1
    bsdec=1
    i=0
    while (abs(bsra) > 0.001 or abs(bsdec) > 0.001) and i < 50:
        ind1,ind2,dist12,bsra,bsdec,bsrasig,bsdecsig,nxi = match2_boresight(ra1, dec1, ra2, dec2, errors=np.sqrt(errors**2+bserr**2),nsigma=nsigma, bsra_init=bsra_tot, bsdec_init=bsdec_tot)
        bsra_tot += bsra*0.8
        bsdec_tot += bsdec*0.8
        print 'Total boresight correction: '+str(round(bsra_tot,5))+' +/- '+str(round(bsrasig,5))+', '+str(round(bsdec_tot,5))+' +/- '+str(round(bsdecsig,5))+' arcsec'
        bserr = np.sqrt(bsrasig**2+bsdecsig**2)
        print 'New Boresight error:',bserr
        i+=1
    return ind1,ind2,dist12, bsra_tot, bsdec_tot, bsrasig, bsdecsig, nxi


#----------
def match2_boresight_map(ra1,dec1,ra2,dec2,errors=.2,nsigma=1., maxsh=5., incr=.5, bsra=0.,bsdec=0.,outfile=''):
    """
    create a map of the number of matches with given offsets

    Input:
    - ra1, dec1: first source list with RA and Dec in degrees
    - ra2, dec2: second source list with RA and Dec in degrees
    - errors: 1 sigma error in arcsec for the most distant match for each source in the second list
    - nsigma: multiply errors by nsigma for matching
    - maxsh: maximum shift for matching
    - incr: steps for shifts
    - bsra: boresight correction in RA
    - bsdec: boresight correction in Dec
    - outfile: if given, save plot with this filename

    Output:
    X, Y: coordinates of the map
    Z: number of matches for each coordinate
    save plot as outfile
    """
    shifts=np.array(range(int(2*maxsh/incr+1)))*incr-maxsh
    szmap=len(shifts)
    X, Y = np.meshgrid(shifts, shifts)
    print 'Size of map:',szmap,'x',szmap
    print 'Shifts:'
    i=0
    bmap=np.zeros([szmap,szmap])
    for i,xi in enumerate(shifts):
        for j,yj in enumerate(shifts):
            print '\n',i+1,'/',szmap,'-',j+1,'/',szmap
            i1,i2,d12, bsra_new, bsdec_new, bsrasig, bsdecsig, nxi = match2_boresight(ra1,dec1,ra2,dec2,errors=errors,nsigma=nsigma,bsra_init=xi+bsra,bsdec_init=yj+bsdec)
            bmap[i,j]=nxi
    Z = bmap
    if outfile != '':
        fn = outfile+'_'+str(maxsh)+'-'+str(incr)+'_'+str(nsigma)+'s'
        match2_boresight_map_save(fn,Z)
        match2_boresight_map_plot(fn,Z, maxsh=maxsh, incr=incr)
        match2_boresight_map_plotprofile(fn+'_profile',Z, maxsh=maxsh, incr=incr)

    return X, Y, Z

        
def match2_boresight_map_plot(outfile, Z, maxsh=5., incr=.5, ftype='png'):
    from matplotlib import cm
    import matplotlib.pyplot as plt
    fig = plt.figure()
    plt.clf()
    imsz = maxsh+.5*incr
    shifts=np.array(range(int(2*maxsh/incr+1)))*incr-maxsh
    X, Y = np.meshgrid(shifts, shifts)
    dist = np.sqrt(X**2+Y**2)
    im = plt.imshow(Z, interpolation='hanning', origin='lower', extent=(-imsz,imsz,-imsz,imsz), cmap=cm.Greys)
    #cm.gist_heat)
    #plt.grid(c='white')
    cb = plt.colorbar()
    plt.xlabel('RA shift (arcsec)')
    plt.ylabel('Dec shift (arcsec)')
    mx=np.max(Z)
    tab = atpy.Table()
    tab.add_column('Z',Z.reshape(Z.size))
    tab.add_column('dist',dist.reshape(dist.size))
    tab.sort('dist')
    Zout=tab.where(tab['dist']>2*incr)['Z']
    med = np.median(Zout)
    sigd = np.std(Zout)
    sigp = np.sqrt(med)
    if sigp<1: sigp=1
    if med>10: sigp=2
    if med>15: sigp=1
    if med>20: sigp=0
    sig = np.sqrt(sigd**2+sigp**2)
    cb.set_label('N of matches (median='+str(round(med,1))+', sigma='+str(round(sig,1))+')')
    clim=cb.get_clim()
    lev=[med,med+sig,med+3*sig]
    colors=['grey','#FF8888','#DD0000']
    cb.ax.plot([0,1],([lev[0]]*2-clim[0])/(clim[1]-clim[0]),'-',color=colors[0],lw=2)
    cb.ax.plot([0,1],([lev[1]]*2-clim[0])/(clim[1]-clim[0]),'-',color=colors[1],lw=2)
    cb.ax.plot([0,1],([lev[2]]*2-clim[0])/(clim[1]-clim[0]),'-',color=colors[2],lw=2)
    CS = plt.contour(X, Y, Z, lev, linestyles=['dotted','solid','solid'],colors=colors)
    #CS2 = plt.contour(X, Y, Z, [med,med+3*sig], cmap=cm.Greys)
    #plt.show()
    print '\nSaving '+outfile+'.'+ftype
    plt.savefig(outfile+'.'+ftype)
    plt.close()


def match2_boresight_map_plotprofile(outfile, Z, maxsh=5., incr=.5, ftype='png'):
    from matplotlib import cm
    import matplotlib.pyplot as plt
    fig = plt.figure()
    plt.clf()
    imsz = maxsh+.5*incr
    shifts = np.array(range(int(2*maxsh/incr+1)))*incr-maxsh
    X, Y = np.meshgrid(shifts, shifts)
    dist = np.sqrt(X**2+Y**2)
    dsign = np.sign(X+.2)
    dsign[:maxsh/incr,maxsh/incr]=-1
    dist *= dsign
    tab = atpy.Table()
    tab.add_column('Z',Z.reshape(Z.size))
    tab.add_column('dist',dist.reshape(dist.size))
    tab.sort('dist')
    distm = [tab['dist'][0]]
    Zm = [tab['Z'][0]]
    ni = 1.
    for r in tab:
        if r[1] == distm[-1]:
            Zm[-1] += r[0]
            ni += 1
        else:
            Zm[-1] = Zm[-1]/ni
            ni=1.
            Zm.append(r[0])
            distm.append(r[1])
    
    #print distm
    #print Zm
    distm=np.array(distm)
    Zm=np.array(Zm)
    fig = plt.figure()
    mx=np.max(Z)
    Zout=tab.where(np.abs(tab['dist'])>2*incr)['Z']
    med = np.median(Zout)
    sigd = np.std(Zout)
    sigp = np.sqrt(med)
    print 'median=',med
    if sigp<1: sigp=1
    if med>10: sigp=2
    if med>15: sigp=1
    if med>20: sigp=0
    sig = np.sqrt(sigd**2+sigp**2)
    print 'sigma=',sig, '(',sigd,',', sigp,')'
    plt.clf()
    plt.plot(distm[:-2],Zm[:-2],'-b',drawstyle='steps-mid',lw=1)
    #plt.plot(-distm[:-2],Zm[:-2],'-b',drawstyle='steps-mid',lw=1)
    plt.plot(tab['dist'],tab['Z'],'.b',ls='None',ms=10)
    #plt.plot(tab['dist'],tab['Z'],'.r',ls='None',ms=10)
    plt.axvline(0,ls='-',color='black',lw=1)
    plt.axhline(med,ls='--',color='black',lw=2)
    plt.axhline(med+sig,ls=':',color='#FF8888',lw=2)
    plt.axhline(med+3*sig,ls=':',color='#DD0000',lw=2)
    plt.axhline(med-sig,ls=':',color='#FF8888',lw=2)
    plt.axhline(med-3*sig,ls=':',color='#DD0000',lw=2)
    plt.xlabel('Distance (arcsec)',fontsize='x-large')
    plt.ylabel('N of matches',fontsize='x-large')
    ylimmin=min(tab['Z'])-1
    if ylimmin<0: ylimmin=0
    ylimmax=max(tab['Z'])+1
    plt.ylim(ylimmin,ylimmax)
    plt.minorticks_on()
    print '\nSaving '+outfile+'.'+ftype
    plt.savefig(outfile+'.'+ftype)
    plt.close()

def match2_boresight_map_save(outfile, Z):
    fn = outfile+'.z.txt'
    print '\nSaving '+fn
    np.savetxt(fn,Z, fmt="%d")

def match2_boresight_map_load(infile, maxsh=None, incr=None):
    try:
        subst = infile.split('_')[-2].split('-')
        maxsh = float(subst[0])
        incr = float(subst[1])
        Z = np.genfromtxt(infile)
        return Z, maxsh, incr
    except:
        print 'Filename does not contain maxsh and incr, eg ..._maxsh-incr_3s.z.txt'


#----------
def match2_separation(ra1,dec1,ra2,dec2,ind):
    """
    Give the separation in arcsec between 2 source lists with the same number of elements and same order

    Input:
    - ra1, dec1: first source list with RA and Dec in degrees
    - ra2, dec2: second source list with RA and Dec in degrees

    Output:
    - dist: array with same number of elements with the separations in arcsec
    """
    ra1=np.array(ra1)
    dec1=np.array(dec1)
    ra2=np.array(ra2)
    dec2=np.array(dec2)
    dist=[]
    for i in range(len(ind)):
        indi=ind[i]
        disti=[]
        for ii in indi:
            disti.append(separation(ra1[ii],dec1[ii],ra2[i],dec2[i])*3600)
        dist.append(disti)
    return dist


#----------
def match3(ra1,dec1,ra2,dec2,ra3,dec3,radius=.6,names=['1','2','3']):
    """
    Match 3 (ra,dec) source lists and return the indices
    Only the best matches withion the circle with given radius are kept

    Input:
    - ra1, dec1: first source list with RA and Dec in degrees
    - ra2, dec2: second source list with RA and Dec in degrees
    - ra3, dec3: third source list with RA and Dec in degrees
    - radius: distance in arcsec for the most distant match

    Output:
    - ind1_123, ind2_123 and ind3_123: indices for each source list for successfull cross match of the 3 source lists
    - dist_123: distance indicator for 3 match sources
    - ind1_12x and ind2_12x: indices for source list 1 and 2 for godd matches only between 1 and 2, and not 3 (1-2 exclusive matches)
    - ind2_23x and ind3_23x: same with source lists 2 and 3
    - ind3_31x and ind1_31x: same with source lists 3 and 1
    - ind1x, ind2x and ind3x: sources without match in each source list
    """
    ra1=np.array(ra1)
    dec1=np.array(dec1)
    ra2=np.array(ra2)
    dec2=np.array(dec2)
    ra3=np.array(ra3)
    dec3=np.array(dec3)
    n1=len(ra1)
    n2=len(ra2)
    n3=len(ra3)
    double = [names[0]+names[1], names[1]+names[2], names[2]+names[0]]
    triple = names[0]+names[1]+names[2]
    print names
    print double
    print triple

    # Match 1-2, 2-3 and 3-1
    print '\nMatching 3 source lists of',str(n1)+',',n2,'and',n3,'elements'
    print '\n* Cross-match '+names[0]+'-'+names[1]
    ind1_12,ind2_12,dist12 = match2(ra1,dec1,ra2,dec2,radius=radius,onlybest='yes')
    print '\n* Cross-match '+names[1]+'-'+names[2]
    ind2_23,ind3_23,dist23 = match2(ra2,dec2,ra3,dec3,radius=radius,onlybest='yes')
    print '\n* Cross-match '+names[2]+'-'+names[0]
    ind3_31,ind1_31,dist31 = match2(ra3,dec3,ra1,dec1,radius=radius,onlybest='yes')

    ind1_123=np.array([],dtype='int64')
    ind2_123=np.array([],dtype='int64')
    ind3_123=np.array([],dtype='int64')
    dist_123=np.array([],dtype='int64')
    # Match 12-3
    print '\n* Cross-match '+double[0]+'-'+names[2]
    ra12=(ra1[ind1_12]+ra2[ind2_12])/2
    dec12=(dec1[ind1_12]+dec2[ind2_12])/2
    ind3_12,ind12_3,dist3_12 = match2(ra3,dec3,ra12,dec12,radius=radius*1.5,onlybest='yes')
    ind1_123=np.append(ind1_123,ind1_12[ind12_3])
    ind2_123=np.append(ind2_123,ind2_12[ind12_3])
    ind3_123=np.append(ind3_123,ind3_12)
    dist3_123=np.column_stack((dist3_12,dist12[ind12_3])).mean(axis=1)
    dist_123=np.append(dist_123,dist3_123)
    # Match 23-1
    print '\n* Cross-match '+double[1]+'-'+names[0]
    ra23=(ra2[ind2_23]+ra3[ind3_23])/2
    dec23=(dec2[ind2_23]+dec3[ind3_23])/2
    ind1_23,ind23_1,dist1_23 = match2(ra1,dec1,ra23,dec23,radius=radius*1.5,onlybest='yes')
    ind1_123=np.append(ind1_123,ind1_23)
    ind2_123=np.append(ind2_123,ind2_23[ind23_1])
    ind3_123=np.append(ind3_123,ind3_23[ind23_1])
    dist1_123=np.column_stack((dist1_23,dist23[ind23_1])).mean(axis=1)
    dist_123=np.append(dist_123,dist1_123)
    # Match 31-2
    print '\n* Cross-match '+double[2]+'-'+names[1]
    ra31=(ra3[ind3_31]+ra1[ind1_31])/2
    dec31=(dec3[ind3_31]+dec1[ind1_31])/2
    ind2_31,ind31_2,dist2_31 = match2(ra2,dec2,ra31,dec31,radius=radius*1.5,onlybest='yes')
    ind1_123=np.append(ind1_123,ind1_31[ind31_2])
    ind2_123=np.append(ind2_123,ind2_31)
    ind3_123=np.append(ind3_123,ind3_31[ind31_2])
    dist2_123=np.column_stack((dist2_31,dist31[ind31_2])).mean(axis=1)
    dist_123=np.append(dist_123,dist2_123)

    #print dist_123

    # Remove multiple matches
    print '\n* Clean duplicated triple matches'
    ind3x = atpy.Table()
    ind3x.add_column(names[0],ind1_123,dtype='int64')
    ind3x.add_column(names[1],ind2_123,dtype='int64')
    ind3x.add_column(names[2],ind3_123,dtype='int64')
    ind3x.add_column('dist',dist_123,dtype='float64')
    for col in names:
        ind3x.sort([col,'dist'])
        keep=[]
        i=0
        n=len(ind3x)
        while i < n:
            v = ind3x.data[col][i]
            keep.append(i)
            i+=1
            while i < n and v == ind3x.data[col][i]: i+=1
        ind3x = ind3x.rows(keep)
        print n, '-->', len(ind3x)
    ind1_123 = ind3x.data[names[0]]
    ind2_123 = ind3x.data[names[1]]
    ind3_123 = ind3x.data[names[2]]
    dist_123 = ind3x.data['dist']

    print '\nFound',len(ind1_123),'triple matches'
    print 'Mean distance =',np.mean(dist_123)

    # 12 23 31 exclusive groups
    print '\n* Compute exclusive complementary groups of double matches'
    ind1_12x, ind2_12x, dist12x = comp2(ind1_123, ind2_123, ind1_12, ind2_12, dist=dist12)
    print names[0]+'-'+names[1]+' matches only:',len(ind1_12x)
    ind2_23x, ind3_23x, dist23x = comp2(ind2_123, ind3_123, ind2_23, ind3_23, dist=dist23)
    print names[1]+'-'+names[2]+' matches only:',len(ind2_23x)
    ind3_31x, ind1_31x, dist31x = comp2(ind3_123, ind1_123, ind3_31, ind1_31, dist=dist31)
    print names[2]+'-'+names[0]+' matches only:',len(ind3_31x)

    # Remove matches in 12 23 31 exclusive groups if already in 123
    print '\n* Clean double matches that are associated to triple matches'
    ind2x={}
    ind2x[double[0]] = atpy.Table()
    ind2x[double[0]].add_column(names[0],ind1_12x,dtype='int64')
    ind2x[double[0]].add_column(names[1],ind2_12x,dtype='int64')
    ind2x[double[0]].add_column('dist',dist12x,dtype='float64')
    ind2x[double[1]] = atpy.Table()
    ind2x[double[1]].add_column(names[1],ind2_23x,dtype='int64')
    ind2x[double[1]].add_column(names[2],ind3_23x,dtype='int64')
    ind2x[double[1]].add_column('dist',dist23x,dtype='float64')
    ind2x[double[2]] = atpy.Table()
    ind2x[double[2]].add_column(names[2],ind3_31x,dtype='int64')
    ind2x[double[2]].add_column(names[0],ind1_31x,dtype='int64')
    ind2x[double[2]].add_column('dist',dist31x,dtype='float64')
    for t in double:
        for col in [t[0],t[1]]:
            keep = clean_duplicates(ind2x[t].data[col],ind3x.data[col],name=col+' in '+t)
            ind2x[t] = ind2x[t].rows(keep)

    # Remove duplicate matches in 12 23 31 exclusive groups
    # if one index is duplicated keep the double match with min distance only
    print '\n* Clean duplicate double matches'
    for t in double:
        for col in [t[0],t[1]]:
            ind2x[t].sort([col,'dist'])
            keep=[]
            i=0
            n=len(ind2x[t])
            while i < n:
                v = ind2x[t].data[col][i]
                keep.append(i)
                i+=1
                while i < n and v == ind2x[t].data[col][i]: i+=1
            ind2x[t] = ind2x[t].rows(keep)
            print n, '-->', len(ind2x[t])


    # 1 2 3 exclusive groups
    ind1x={}
    print '\n* Compute exclusive complementary groups of single sources'
    ind1x[names[0]] = comp(np.append(np.append(ind1_123,ind2x[double[0]].data[names[0]]),ind2x[double[2]].data[names[0]]), range(n1))
    print 'sources only detected in '+names[0]+':',len(ind1x[names[0]])
    #ind1x[names[1]]=comp(np.append(np.append(ind2_123,ind2_23x),ind2_12x),range(n2))
    ind1x[names[1]] = comp(np.append(np.append(ind2_123,ind2x[double[1]].data[names[1]]),ind2x[double[0]].data[names[1]]), range(n2))
    print 'sources only detected in '+names[1]+':',len(ind1x[names[1]])
    #ind1x[names[2]]=comp(np.append(np.append(ind3_123,ind3_31x),ind3_23x),range(n3))
    ind1x[names[2]] = comp(np.append(np.append(ind3_123,ind2x[double[2]].data[names[2]]),ind2x[double[1]].data[names[2]]), range(n3))
    print 'sources only detected in '+names[2]+':',len(ind1x[names[2]]),'\n'

    #return ind1_123,ind2_123,ind3_123, dist_123, ind1_12x,ind2_12x,ind2_23x,ind3_23x,ind3_31x,ind1_31x, ind1x,ind2x,ind3x
    return ind3x,ind2x,ind1x

def clean_duplicates(ind,indref,name=''):
    keep=[]
    for i,ii in enumerate(ind):
        if ii not in indref: keep.append(i)
    print name+': '+str(len(ind)-len(keep))+' duplicates'
    return keep

def comp(ind, allind):
    """
    Return the complement of the array ind in the group allind
    """
    comp=np.array([],dtype='int64')
    for v in allind:
        if not v in ind: comp=np.append(comp,v)
    return np.array(comp)

def comp2(ind1, ind2, allind1, allind2, dist=None):
    """
    Return the complement (comp1,comp2) of the array (ind1,ind2) in the group (allind1,allind2)
    """
    from scipy.spatial import KDTree
    comp=np.array(zip(allind1,allind2))
    tree1=KDTree(zip(ind1,ind2))
    n=len(ind1)
    keep=[]
    for i,c in enumerate(comp):
        md,mi=tree1.query(c, distance_upper_bound=.99)
        if mi == n:
            keep.append(i)
    comp=comp[keep]
    #comp=[]
    #ind=zip(ind1,ind2)
    ##allind=np.column_stack((allind1,allind2))
    #allind=zip(allind1,allind2)
    #comp=allind
    #for i,v in enumerate(ind):
        #if v in comp:
            ##comp.append(list(v))
            #del comp[comp.index(v)]
    comp=np.transpose(np.array(comp))
    if dist != None:
        return np.array(comp[0],dtype='int64'), np.array(comp[1],dtype='int64'), dist[keep]
    else:
        return np.array(comp[0],dtype='int64'), np.array(comp[1],dtype='int64')

ind1=[1,3,5,6]
ind2=[2,3,6,7]
allind1=[1,2,3,4,5,6,7,8,9]
allind2=allind1




# ---------- DS9 THUMBS


def ds9_load(fnames, regs, pathin='.', height=400, width=1200, vlayout=False, mode='column', grid=None, zoom=4, scale=False, scalemode=False, showtext=1):
    import ds9
    d = ds9.ds9()
    d.set('frame delete all')
    if vlayout: d.set('view layout vertical')
    else: d.set('view layout horizontal')
    d.set('tile mode '+mode)
    if (mode=='grid')&(grid!=None):
        d.set('tile grid manual')
        d.set('tile grid layout '+str(grid[0])+' '+str(grid[1]))
    d.set('height '+str(height))
    d.set('width '+str(width))
    d.set('tile yes')
    for f in fnames:
        if f != '':
            print 'loading image '+f
            d.set('file new '+pathin+'/'+f)
            if scalemode: d.set('scale mode '+str(scalemode))
            if scale: d.set('scale '+str(scale))
            d.set('zoom to '+str(zoom))
            if 'conv_' in f:
                d.set('scale limits -5000 5000')
                #d.set('scale mode zscale')
            #d.set('regions '+pathin+'/'+f.split('.fit')[0]+'_mag.reg')
            for r in regs:
                print 'loading regions '+r
                d.set('regions '+pathin+'/'+r)
                if showtext == 0: d.set('regions showtext no')
        else:
            d.set('frame new')
    #d.set('frame new')
    d.set('frame first')
    return d


def ds9_thumbs(d, srclist, height=400, width=1200, movie=0, pathout='.'):
    '''
    Open images (fnames) in DS9, with region files (regs)
    For each source in the disctionnary srclist, center and save an image
    srclist: dictionnary of coordinates (format: key,[RA, Dec])
    fnames: list of fits file names
    regs: list of region file names
    '''
    for xsrc in srclist.keys():
        ra = srclist[xsrc][0]
        dec = srclist[xsrc][1]
        print xsrc,ra,dec
        d.set('pan to '+str(ra)+' '+str(dec)+' wcs fk5')
        d.set('match frames wcs')
        if movie == 1:
            d.set('height '+str(int(height/3.)))
            d.set('width '+str(int(width/3.)))
            d.set('savempeg '+pathout+'/'+str(xsrc)+'.mpg')
        d.set('height '+str(height))
        d.set('width '+str(width))
        d.set('saveimage png '+pathout+'/'+str(xsrc)+'.png')

def ds9_center(d, ra, dec):
    '''
    Center on ra/dec position
    '''
    d.set('pan to '+str(ra)+' '+str(dec)+' wcs fk5')
    d.set('match frames wcs')

def ds9_saveimg(d, srcname, pathout='.'):
    d.set('saveimage png '+pathout+'/'+str(srcname)+'.png')



#----------
#if __name__ == "__main__":
    #separation(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])

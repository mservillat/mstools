
#IRAF spec extract

import os,glob,sys,string
import pyfits
import numpy as np
from pyraf import iraf

rdnoise=0.
gain=1.
observatory='paranal'
extinction='onedstds$ctioextinct.dat'
caldir='onedstds$ctionewcal/'

# Prepare Slit image

def cut_slit2d(flist,xrng,yrng,slitname='slit'):
    """
    Extract slit region from different images
    """
    print '\nExtract slit regions'
    for f in flist:
        fin = f+'['+str(xrng[0])+':'+str(xrng[1])+','+str(yrng[0])+':'+str(yrng[1])+']'
        fout = f.split('.fits')[0]+'_'+slitname+'.fits'
        if os.path.isfile(fout): os.remove(fout)
        iraf.imcopy(fin,fout)
    

def extract_slit2d(flist,slit,docopy=True,addslitname=False):
    """
    Extract each slit image from multi extension file
    Return the list of created files
    """
    print '\nExtract slit images'
    spobj = pyfits.getval(flist[0],'object',ext=slit)
    flist_slit=[]
    for f in flist:
        for s in [slit]:
            fin = f+'['+str(s)+']'
            fout = f.split('.fits')[0]+'_'+str(s).zfill(2)
            if addslitname: fout = fout+'_'+spobj
            fout = fout+'.fits'
            flist_slit.append(fout)
            if docopy:
                if os.path.isfile(fout): os.remove(fout)
                iraf.imcopy(fin,fout)
    return flist_slit


def skysub_slit2d(sp2d,axis=0,lrange=[0,1]):
    """
    Estimate and subtract sky of a slit image
    """
    print '\nSubtract sky for '+sp2d
    hdu = pyfits.open(sp2d)
    hdu0 = hdu[0]
    if axis == 0: subsp = hdu0.data[lrange[0]:lrange[1],:]
    else: subsp = hdu0.data[:,lrange[0]:lrange[1]]
    sky = np.median(subsp,axis=axis)
    if axis == 0:
        for i in range(hdu0.data.shape[axis]):
            hdu0.data[i,:] = hdu0.data[i,:] - sky
    else:
        for i in range(hdu0.data.shape[axis]):
            hdu0.data[:,i] = hdu0.data[:,i] - sky
    fout = sp2d.split('.fits')[0]+'.subsky.fits'
    hdu.writeto(fout,clobber=True)



def combine_slit2d(flist,slit,name,addslitname=True):
    """
    Combine several slit images and remove cosmic rays
    """
    print '\nCombine '+str(len(flist))+' files'
    print flist
    spobj = pyfits.getval(flist[0],'object')
    fname = name+'_2spec_'+str(slit).zfill(2)
    if addslitname: fname = fname+'_'+spobj
    fname = fname+'.fits'
    if os.path.isfile(fname): os.remove(fname)
    iraf.imcombine(','.join(flist), fname, combine='average', reject='pclip')
    return fname

# Main extraction steps (aperture, trace, wav calib, flux calib)

def extract_aper(sp,cp, references=None, doextract=True, line=20, nsum=10, dowavcalib=True, linelist='linelists$idhenear.dat', dofluxcalib=False, sensfile='../sens', interactive='yes', secondpass=False, doview=True):
    """
    Extract spectra in apertures from slit image
    """
    global rdnoise, gain, observatory
    print '\nExtract spectrum from 2D spectrum'
    print 'spec:',sp
    print 'comp:',cp
    spred = sp.split('.fits')[0]+'.ms.fits'
    if doextract:
        print '-- Extract with apall'
        if references:
            print 'ref :',references
            iraf.apall(sp, references=references, edit='no', trace='no', interactive=interactive, gain=gain, readnoise=rdnoise, line=line, nsum=nsum)
        else:
            iraf.apedit(sp, edit='yes', line=line, nsum=nsum)
            iraf.aptrace(sp, edit='no', line=line)
            iraf.apall(sp, gain=gain, readnoise=rdnoise, edit='no', trace='no', line=line, nsum=nsum)
        redo = 'n'
        if secondpass and interactive == 'yes':
            print '\nCheck aperture database, redo? (y,n) '
            redo = sys.stdin.read(1)
        if redo == 'y':
            iraf.apall(sp,edit='no',trace='no', line=line, nsum=nsum)
    spred_nowav = spred
    spred = spred_nowav.split('.fits')[0]+'.wav.fits'
    if dowavcalib:
        print '-- Wav calib with identify'
        # check the comp exposure, use l then g/f to get shift and RMS
        cpred = cp.split('.fits')[0]+'.ms.fits'
        if os.path.isfile(cpred): os.remove(cpred)
        iraf.apall(cp, references=sp, edit='no', background='no', trace='no', interactive='no')
        ncp=1
        if 'NAXIS2' in pyfits.getheader(cpred).keys():
            ncp = pyfits.getval(cpred,'NAXIS2')
        print 'Identify lines for wavelength calibration (use m/l,f,q) -- '+str(ncp)+' apertures'
        for icp in range(1,ncp+1):
            iraf.identify(cpred, section='line '+str(icp), coordlist=linelist, autowrite='yes')
        iraf.refspectra(spred_nowav, references=cpred, select='nearest', sort='', group='', answer='YES')
        if os.path.isfile(spred): os.remove(spred)
        iraf.dispcor(spred_nowav,spred)
    if dofluxcalib:
        print '-- Flux calib with calibrate and '+sensfile+' (see make_sens)'
        spred_noflux = spred
        spred = spred_noflux.split('.fits')[0]+'.flux.fits'
        if os.path.isfile(spred): os.remove(spred)
        iraf.calibrate(spred_noflux, spred, sensitivity=sensfile, observatory=observatory, extinction=extinction, ignoreaps='yes')
    if doview:
        iraf.splot(spred,1,1)
    return spred

# prepare flux calib (reduce standard, then make_sens)

def make_sens(stdsp,star_name,output_std='std',output_sens='sens'):
    """
    Estimate sensitivity response from standard spectrum
    """
    if os.path.isfile(output_std):
        print 'Remove:',output_std
        os.remove(output_std)
    iraf.standard(stdsp, output_std, star_name=star_name, extinction=extinction, caldir=caldir)
    print 'Saved:',output_std,'\n'
    slist = glob.glob(output_sens+'*')
    for s in slist:
        if os.path.isfile(output_std):
            print 'Remove:',s
            os.remove(s)
    iraf.sensfunc(output_std, output_sens, ignoreaps='yes', extinction=extinction, newextinctio="extinct.dat")
    print 'Saved:'
    print glob.glob(output_sens+'*')
    

# generate final files

def get_each_sp(spred,unit_wav='A',unit_flux='erg/cm2/s/A',frames=[1]):
    """
    Create individual spectra from multi file in fits, dat and txt format
    """
    print '\nExtract each spectrum in file '+spred
    nsp=1
    if 'NAXIS2' in pyfits.getheader(spred).keys():
       nsp = pyfits.getval(spred,'NAXIS2')
    spobj = pyfits.getval(spred,'object')
    for i in range(1,nsp+1):
        for j in frames:
            fin = spred+'[*,'+str(i)+','+str(j)+']'
            fout = spred.split('.fits')[0]+'.'+str(i)+'.'+str(j)+'.fits'
            if os.path.isfile(fout): os.remove(fout)
            iraf.imcopy(fin,fout)
            fout2 = spred.split('.fits')[0]+'.'+str(i)+'.'+str(j)+'.dat'
            iraf.wspectext(fout,fout2,header='no')
            fout3 = spred.split('.fits')[0]+'.'+str(i)+'.'+str(j)+'.txt'
            fin2 = open(fout2,'r')
            fout3_disk = open(fout3,'w')
            if spobj[0] in string.digits: spobj = 'src'+spobj
            fout3_disk.write(spobj+'\n')
            fout3_disk.write(unit_wav+'\n')
            fout3_disk.write(unit_flux+'\n')
            fout3_disk.writelines(fin2.readlines())
            fin2.close()
            fout3_disk.close()

# checks

def check_sky(skysp):
    """
    Check the sky lines, use e/l then g/f to get shift and RMS
    """
    iraf.identify(skysp, coordlist='linelists$skylines.dat', function='legendre', order=2)


def check_comp(compsp):
    """
    Check the comparison lamp lines, use e/l then g/f to get shift and RMS
    """
    iraf.identify(compsp, coordlist='linelists$henear.dat', function='legendre', order=2)


# Pipeline

def doall_combine(name,splist,comps,slits, references=None, docopy=True, interactive='yes', dowav=True, doflux=True,sensfile='../sens', addslitname=False,secondpass=False,unit_wav='A',unit_flux='erg/cm2/s/A'):
    """
    From combines 2d slit images to individual spectra
    * combine all slit images with extract_2dslit and combine_2dslit
    * extract spectra from defined aperture with extract_aper
    * get individual spectrum files with get_each_sp
    """
    for s in slits:
        splist_slit = extract_slit2d(splist, s, docopy=docopy, addslitname=addslitname)
        comps_slit = extract_slit2d(comps , s, docopy=docopy, addslitname=addslitname)
        sp = combine_slit2d(splist_slit,s,name,addslitname=addslitname)
        cp = combine_slit2d(comps_slit,s,'comp',addslitname=addslitname)
        if references: ref = references
        else: ref = None
        spred = extract_aper(sp,cp, references=ref, dowavcalib=dowav, dofluxcalib=doflux, sensfile=sensfile, interactive=interactive, secondpass=secondpass)
        get_each_sp(spred, unit_wav=unit_wav,unit_flux=unit_flux)


def get_references(name,splist,slits, addslitname=True):
    """
    Create list of filenames that will be use as a reference for aperture extraction and line identification
    """
    ref=[]
    for i,s in enumerate(slits):
        spobj = pyfits.getval(splist[0],'object',ext=s)
        refname = name+'_2spec_'+str(s).zfill(2)
        if addslitname: refname = refname+'_'+spobj
        refname = refname+'.fits'
        ref.append(refname)
    return ref


def doeach(name, splist, comps, slits, references=None, docopy=False, interactive='no', dowav=True, doflux=True,sensfile='../sens', addslitname=False, secondpass=False,unit_wav='A',unit_flux='erg/cm2/s/A'):
    """
    Same as doall_combine for each slit image
    Generally run doall_combine first and use the combined files as a reference
    """
    for i,s in enumerate(slits):
        splist_slit = extract_slit2d(splist,s,docopy=docopy,addslitname=addslitname)
        comps_slit = extract_slit2d(comps,s,docopy=docopy,addslitname=addslitname)
        for j,sp in enumerate(splist_slit):
            if references: ref = references[i]
            else: ref = None
            spred = extract_aper(sp, comps_slit[j], references=ref, dowavcalib=dowav, dofluxcalib=doflux, sensfile=sensfile, interactive=interactive, secondpass=secondpass)
            get_each_sp(spred, unit_wav=unit_wav,unit_flux=unit_flux)


def doall(name,splist,comps,slits,addslitname=False):
    """
    Quick call to doall_combine and doeach
    """
    doall_combine(name,splist,comps,slits,addslitname=addslitname)
    refs = specextract.get_references(name,splist,slits,addslitname=addslitname)
    doeach(name,splist,comps,slits,references=ref,addslitname=addslitnames)


# PLOT in python

def plot_lines(wav,lab,yval=0,xoffset=0,ls='--',lw=1,lc='k',fs=10,rot=90):
    for i in range(len(wav)):
        axvline(wav[i],ls=ls,lw=lw,color=lc)
        text(wav[i],yval,lab[i],fs=fs,rotation=rot)







#! /usr/bin/env python
#
# Functions to analyse data
#
# 10 Apr 2012: Mathieu Servillat

import os
import numpy as np
import atpy, asciitable
#from astLib import astCoords
#import astroTools

# Return the lowess fit vector with window f using the R function
def lowess(xv,yv,f=0.66):
    import rpy2.robjects as ro
    yfitall = np.array(ro.r.lowess(ro.FloatVector(xv),ro.FloatVector(yv), f=f))
    return yfitall[1]

# Return the lowess fit vector with window f using the R function
def detrend(xv,yv,f=0.66):
    print 'timescale:',f*(max(xv)-min(xv))
    yfit = lowess(xv,yv,f=f)
    yvd = yv / yfit
    return yvd

# Lomb scargle periodogram + significance
def lombscargle(time,flx,pinput,prob=0.99,simu=False,nsimu=100,signi_def=0.):
    import scipy.signal.spectral
    mx = np.mean(flx)
    vx = np.var(flx)
    flx = flx - mx
    flx = flx / np.std(flx)
    #print mx, vx, '--> 0 1'
    nt = len(time)
    nper = len(pinput)
    ndat = len(time)
    #varflux = mean(errflux)**2
    #normpgram = lambda x: x
    #sqrt(4*(x/normval))
    # False Alarm Probability according to nper (99%)
    freq = 1./pinput
    xmin=min(time);
    xmax=max(time);
    xdif=xmax-xmin;
    fc = ndat/(2.*xdif) # "average" Nyquist frequency
    hifac=max(freq)/fc
    #nout=fix(0.5*ofac*hifac*ndat)
    ofac = 2*nper / (hifac*ndat)
    effm = 2*nper / ofac
    # number of independent frequ, Horne & Baliunas (1986, ApJ, 302, 757)
    ndat2 = -6.362 + 1.193*ndat + 0.00098*ndat**2
    print ndat
    print ndat2
    signi = -np.log( 1. - ((1.-(1-prob))**(1./ndat)) ) # from Scargle
    #signi = nt/2.*( 1. - ((1.-(1.-prob))**(1./nper))**(2./(nt-3.)) )
    if signi_def != 0:
        print 'Set signi to given default'
        signi=signi_def
    if simu:
        print 'Get signi from simulation (nsimu =',nsimu,')'
        signi = simu_lombscargle(time,pinput,prob=prob,nsimu=nsimu)
    px = scipy.signal.spectral.lombscargle(time, flx, 2*np.pi/pinput)
    return px, signi

def simu_lombscargle(time,pinput,prob=0.99,nsimu=100):
    pgpeaks = []
    for i in range(nsimu):
        flx = np.random.randn(len(time))
        px, signi = lombscargle(np.array(time,dtype='float64'),flx,pinput,prob=0.99)
        pgpeaks.append(max(px))
        print pgpeaks[-1]
    pgpeaks.sort()
    return pgpeaks[int(prob*(nsimu-1))]

# smooth vector
def smooth(x,window_len=11,window='hanning'):
    """smooth the data using a window with requested size.
    This method is based on the convolution of a scaled window with the signal.
    The signal is prepared by introducing reflected copies of the signal
    (with the window size) in both ends so that transient parts are minimized
    in the begining and end part of the output signal.
    input:
        x: the input signal
        window_len: the dimension of the smoothing window; should be an odd integer
        window: the type of window from 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'
            flat window will produce a moving average smoothing.
    output:
        the smoothed signal

    example:

    t=linspace(-2,2,0.1)
    x=sin(t)+randn(len(t))*0.1
    y=smooth(x)

    see also:

    numpy.hanning, numpy.hamming, numpy.bartlett, numpy.blackman, numpy.convolve
    scipy.signal.lfilter
    TODO: the window parameter could be the window itself if an array instead of a string
    """

    import copy

    if x.ndim != 1:
        raise ValueError, "smooth only accepts 1 dimension arrays."

    if x.size < window_len:
         raise ValueError, "Input vector needs to be bigger than window size."

    if window_len<3:
        return x

    if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman', 'median']:
        raise ValueError, "Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman', 'median'"

    #s=numpy.r_[ 2*x[0]-x[window_len:1:-1], x, 2*x[-1]-x[-1:-window_len:-1] ]
    s = numpy.r_[ 2*x[0]-x[window_len-1:0:-1], x, 2*x[-1]-x[-2:-window_len-1:-1] ]
    #print(len(s))

    if window == 'median':
	y=copy.deepcopy(s)
	for i in range(window_len-1,x.size+window_len-1):
	    w = round((window_len-1)/2)
	    y[i] = numpy.median(s[i-w:i+w+1])
    else:
	if window == 'flat': #moving average
	    w = numpy.ones(window_len,'d')
	else:
	    w = eval('numpy.'+window+'(window_len)')
	y = numpy.convolve(w/w.sum(),s,mode='same')

    return y[window_len-1:-window_len+1]



# smooth image with median filter
def smooth2D(x,boxsize=5):
    import copy
    y = copy.deepcopy(x)
    w = int((boxsize-1)/2)
    for i in range(w,x.shape[0]-w-1):
	for j in range(w,x.shape[1]-w-1):
	    sub = x[i-w:i+w+1,j-w:j+w+1]
	    y[i,j] = np.median(sub)
    return y

